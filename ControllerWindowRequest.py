from myCmd.ViewTerminalWindow import TerminalWindow
from myCmd.ViewTerminalWindow import DisplayTextCommandOutput
from myCmd.ViewTerminalWindow import DisplayTextCommandInput
from myCmd.ModelProcessRequestOS import Backend
from myCmd.KeyBindEvents import KeyBinds
import tkinter as tk


class Controller:

    command_input = ""
    print_output_from_command = ['', 0.0]
    line_count_entry_text_field = 7.0
    command_list = []
    count_commands_in_list = 0.0

    def __init__(self):
        # Create the model class
        self.bk_end = Backend(user="Matthew")

        # Create the view terminal
        self.terminal_window = TerminalWindow(
            window_title='Terminal', window_dimensions='1000x500+200+200', background='black', title_bar_custom=True
        )

        # Create the output field
        self.output_field = DisplayTextCommandOutput(
            self.terminal_window, height=self.terminal_window.winfo_screenheight() // 5,
            width=self.terminal_window.winfo_screenwidth() // 5, bg='black', fg='#00ff00', font=("Raster Fonts", 14),
            relief=tk.FLAT, wrap=tk.NONE, start_message=self.bk_end.welcome_message,
            current_dir=self.bk_end.current_directory
        )

        # Create the input field
        self.input_field = DisplayTextCommandInput(
            self.terminal_window, height=1, width=100, bg='black', fg='#00ff00', relief=tk.FLAT,
            font=("Raster Fonts", 14), insertbackground='#00ff00', output_field=self.output_field
        )

        # Create bindings for events for an input field
        self.key_binder = KeyBinds()
        self.key_binder.bind_key(widget=self.input_field, key_code='<Return>', binding_function=self.get_input)
        self.key_binder.bind_key(widget=self.input_field, key_code='<Up>', binding_function=self.recall_last,
                                 arg_to_pass_to_function="UP")
        self.key_binder.bind_key(widget=self.input_field, key_code='<Down>', binding_function=self.recall_last,
                                 arg_to_pass_to_function="DOWN")

        self.my_func_dict = {
            'cd': self.bk_end.change_current_directory,
            'dir': self.bk_end.list_current_directory,
            'type': self.bk_end.cat_file_contents,
            'cat': self.bk_end.cat_file_contents,
            'alarm': self.bk_end.start_an_alarm,
            'mkdir': self.bk_end.make_directory,
            'rmdir': self.bk_end.remove_directory,
            'set': self.bk_end.get_environement_variables,
            'not a command': self.bk_end.not_a_command,
        }

    # Key Bind Functions
    def get_input(self, event, args_passed):

        # Get the command as a string
        command_input = event.widget.get(1.0, tk.END).strip('\n')

        # Setup cycling through the list of previously entered commands
        self.command_list.append(command_input)
        self.count_commands_in_list = len(self.command_list)

        # Split the command into ['command', 'target']
        command_and_target_list = self.bk_end.find_command_to_execute(command_input)
        print(command_and_target_list)

        # Run the two arg command and pass a print out object **Not ideal**
        if command_and_target_list[0] in self.my_func_dict:
            self.my_func_dict[command_and_target_list[0]](command_and_target_list[1], self.print_output_from_command)
        else:
            self.my_func_dict['not a command'](command_and_target_list[0], self.print_output_from_command)

        if self.bk_end.error_message == "":
            # Setup the next prompt
            command_input = command_input + '\n\n' + self.print_output_from_command[0] + \
                            self.bk_end.current_directory + ' '
        else:
            command_input = command_input + '\n' + self.bk_end.error_message + self.bk_end.current_directory + ' '
            self.line_count_entry_text_field += 2.0

        # Prepare the text widget to push commands down
        self.output_field.config(state=tk.NORMAL)
        self.output_field.insert(self.line_count_entry_text_field, command_input)
        self.output_field.window_create(window=event.widget, index=tk.END)

        #  TODO FIX
        #  Some smelly code to make the bar auto scroll
        event.widget.config(width=0)
        self.output_field.see(tk.END)
        event.widget.config(width=100)

        self.output_field.config(state=tk.DISABLED)
        self.line_count_entry_text_field += 2.0 + self.print_output_from_command[1]

        event.widget.delete(1.0, tk.END)
        self.print_output_from_command[0] = ""
        self.print_output_from_command[1] = 0.0
        # print(Controller.command_list)

    def recall_last(self, event, key):

        if key == "UP":
            if self.count_commands_in_list > 0:
                event.widget.delete(1.0, tk.END)
                event.widget.insert(1.0, self.command_list[int(self.count_commands_in_list - 1)])
                self.count_commands_in_list -= 1
        else:
            if self.count_commands_in_list < len(self.command_list) - 1:
                event.widget.delete(1.0, tk.END)
                event.widget.insert(1.0, self.command_list[int(self.count_commands_in_list + 1)])
                self.count_commands_in_list += 1

    def run(self):
        self.terminal_window.mainloop()


def main():
    controller = Controller()
    controller.run()

if __name__ == '__main__':
    main()