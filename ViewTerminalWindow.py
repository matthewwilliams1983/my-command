import tkinter as tk
import html
from myCmd.KeyBindEvents import KeyBinds


class TerminalWindow(tk.Tk):

    def __init__(self, *args, window_title='Tkinter', window_dimensions='100x100', background='white',
                 title_bar_custom=False, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)

        # TODO Write own manager
        self.wm_overrideredirect(title_bar_custom)
        self.title(window_title)
        self.geometry(window_dimensions)
        self.configure(bg=background)

        if title_bar_custom:
            self.title_bar = CustomTitleBar(self, bg='white', height=30, window=self)


class CustomTitleBar(tk.Frame):

    def __init__(self, *args, window=None, **kwargs):
        tk.Frame.__init__(self, *args, **kwargs)

        # TODO: Fix typing expanding title bar down
        self.pack(expand=1, fill=tk.X, anchor=tk.N)
        self.parent = window

        self.close_button = tk.Button(self, text=html.unescape('&#10060'), bg='white', relief=tk.FLAT,
                                      font=('Times', 10), fg='grey')
        self.close_button.pack(anchor=tk.E)

        self.off_set_x = 0
        self.off_set_y = 0

        # Create key binds for custom tool bar
        self.key_binder = KeyBinds()
        self.key_binder.bind_key(widget=self, key_code='<Button-1>',
                                 binding_function=self.left_click_title_bar)
        self.key_binder.bind_key(widget=self, key_code='<B1-Motion>', binding_function=self.move_window)
        self.key_binder.bind_key(widget=self.close_button, key_code='<Enter>',
                                 binding_function=self.change_button_background_enter)
        self.key_binder.bind_key(widget=self.close_button, key_code='<Leave>',
                                 binding_function=self.change_button_background_leave)
        self.key_binder.bind_key(widget=self.close_button, key_code='<Button-1>', binding_function=self.close_window)

    def left_click_title_bar(self, event, args_passed):
        self.off_set_x = event.x
        self.off_set_y = event.y

    def move_window(self, event, args_passed):
        x = self.parent.winfo_pointerx() - self.off_set_x
        y = self.parent.winfo_pointery() - self.off_set_y

        self.parent.geometry("+{x}+{y}".format(x=x, y=y))

    def close_window(self, event, args_passed):
        self.parent.destroy()

    def change_button_background_enter(self, event, args_passed):
        self.close_button.config(bg='#ee1111', fg='white')

    def change_button_background_leave(self, event, args_passed):
        self.close_button.config(bg='white', fg='grey')


class DisplayTextCommandOutput(tk.Text):

    def __init__(self, *args, start_message=None, current_dir=None, **kwargs):
        tk.Text.__init__(self, *args, **kwargs)

        self.insert(tk.CURRENT, start_message)
        self.insert(tk.CURRENT, current_dir + ' ')
        self.pack(fill=tk.Y)


class DisplayTextCommandInput(tk.Text):

    def __init__(self, *args, output_field=None, **kwargs):
        tk.Text.__init__(self, *args, **kwargs)

        self.focus()
        output_field.window_create(window=self, index=tk.END)
        output_field.config(state=tk.DISABLED)
