import os
import sys
import datetime
import time
from threading import Timer
from myCmd.InputStringOps import InputStringOps
import wave

#  Not using list, but cool - side note many unix commands not available in windows
#  1)  os.fchdir(fd) gets an opened directory
#  2)  os.getenv(varname[, value]) get the value of the varname eg USERDOMAIN...might be able to use
#  3)  os.putenv(varname, value) sets the environment variable named varname to the string value
#  4)  os.umask(mask) Set the current numeric umask and return the previous umask - file permissions? chmod?
#  5)  os.unsetenv(varname) Unset(delete) the environment variable named varname.
#  6)  os.fdopen(fd[,mode[, bufsize]]) returns file object - use open instead


class Backend:

    def __init__(self, user=None):
        self.__user = user
        self.__os_name = os.name
        self.__sys_platform = sys.platform
        self.__welcome_message = "System platform: {}\nOperating System Type: {}\nCurrent date and time: {:%m/%d/%y  " \
                                 "%H:%M}\nWelcome {}.\n\n".format(self.__os_name, self.__sys_platform,
                                                                  datetime.datetime.now(), self.__user)
        self.__error_message = None
        self.__current_directory = None
        self.__command_input_regex = None

        self.__command_word_regex_search_space = r"(cd|dir|type|cat|alarm|mkdir|rmdir|set)"
        self.__string_operator = InputStringOps(where_to_split=self.__command_word_regex_search_space)
        self.__alarm_timer = 0

    @property
    def welcome_message(self):
        return self.__welcome_message

    @welcome_message.setter
    def welcome_message(self, welcome_message):
        self.__welcome_message = welcome_message

    @property
    def error_message(self):
        return self.__error_message

    @property
    def current_directory(self):
        self.__current_directory = os.getcwd()
        return self.__current_directory

    def find_command_to_execute(self, command_input):

        self.__command_input_regex = self.__string_operator.split_string_in_two(command_input)

        return self.__command_input_regex

    def change_current_directory(self, the_new_directory, print_out):

        # Set error message to nothing
        self.__error_message = ""

        if the_new_directory == "":
            print_out[0] = self.__current_directory + '\n\n'
            print_out[1] = 2
        else:
            try:
                os.chdir(the_new_directory)  # Try to change the directory
            except OSError:
                self.__error_message = "OSError: [WinError 123] The filename, directory name, or volume label " \
                                "syntax is incorrect\n\n"

    def list_current_directory(self, potential_path, print_out):

        self.__error_message = ""
        list_string_for_print = ""

        if potential_path == "":
            potential_path = self.__current_directory

        try:
            temp = os.listdir(potential_path)
            list_string_for_print = list_string_for_print + " Directory of " + potential_path + '\n\n'

            for item in temp:
                file_structure_time = \
                    datetime.datetime.strptime(time.ctime(os.path.getctime(potential_path + '\\' + item)),
                                               "%a %b %d %H:%M:%S %Y")

                file_creation_date_and_time = "{:02d}/{:02d}/{}   {:02d}:{:02d}   ".format(file_structure_time.month,
                                                                                           file_structure_time.day,
                                                                                           str(file_structure_time.year)
                                                                                           ,file_structure_time.hour,
                                                                                           file_structure_time.minute)
                # TODO fix file size output format
                if os.path.isdir(potential_path + '\\' + item):
                    is_dir = "{}".format("<DIR>   ")
                    #file_size = "{}".format("          ")
                else:
                    is_dir = "{}".format("             ")
                    #file_size = "{:>5}  ".format(str(os.stat(potential_path + '\\' + item).st_size))

                list_string_for_print = list_string_for_print + file_creation_date_and_time + is_dir + item + '\n'

            list_string_for_print = list_string_for_print + '\n'

            print_out[0] = list_string_for_print
            print_out[1] = len(temp) + 3
        except FileNotFoundError:
            self.__error_message = potential_path + " is not a valid directory.\n\n"

    def cat_file_contents(self, the_filename, print_out):

        self.__error_message = ""

        if the_filename != "":
            try:
                file_contents = open(the_filename, 'r').read()
                file_contents = file_contents + '\n\n'
                line_count = file_contents.count('\n')
                print_out[0] = file_contents
                print_out[1] = line_count
            except IOError:
                self.__error_message = "The system cannot find the file specified\n\n"
        else:
            self.__error_message = "The system cannot find the file specified\n\n"

    def not_a_command(self, the_command, print_out):

        self.__error_message = the_command + " is not a valid or recognized command\n\n"

        return self.__error_message

    def start_an_alarm(self, alarm_time, print_out):

        self.__error_message = ""

        if float(alarm_time) > 0:
            time_in_minutes = float(alarm_time) * 60.0
            clock_start = Timer(time_in_minutes, self.play_alarm)
            clock_start.start()
        else:
            self.__error_message = "Alarm could not be set at this time. Try again later...\n\n"

    def make_directory(self, directory_name, command):

        self.__error_message = ""

        try:
            os.mkdir(directory_name)
        except FileExistsError:
            self.__error_message = "Cannot create a file when that file already exists: {}\n\n".format(directory_name)

    def remove_directory(self, directory_name, command):

        self.__error_message = ""
        try:
            os.rmdir(directory_name)
        except FileNotFoundError:
            self.__error_message = "The system cannot find the directory named: {}\n\n".format(directory_name)

    #TODO Need to seperate individual entries also line spacing is off
    @staticmethod
    def get_environement_variables(self, print_out):
        current_environment_vars = str(os.environ) + '\n'
        print_out[0] = current_environment_vars
        print_out[1] = len(current_environment_vars) + 3

    @staticmethod
    def play_alarm():

        import pyaudio

        chunk = 1024
        wf = wave.open(r'C:\Users\Matt\Desktop\PythonProjects\myCmd\DigitalAlarmSound.wav', 'rb')
        p = pyaudio.PyAudio()
        stream = p.open(format=p.get_format_from_width(wf.getsampwidth()),
                        channels=wf.getnchannels(),
                        rate=wf.getframerate(),
                        output=True)

        # read data
        data = wf.readframes(chunk)

        # play stream (3)
        while len(data) > 0:
            stream.write(data)
            data = wf.readframes(chunk)

        # stop stream (4)
        stream.stop_stream()
        stream.close()

        # close PyAudio (5)
        p.terminate()

def type_platform_os():

    print('============================================')
    print('\nOperating system platform:', sys.platform)
    print('\nOperating system type:', os.name, "\n")
    print('============================================')


def environment_variables():

    print('============================================')
    print('\nSystem environment variables: ', os.environ, "\n")
    print('============================================')
    #  To do... can I change my environment variables from here? I think so


def get_the_current_process_id():

    print('============================================')
    print('\nCurrent process ID: ', os.getpid(), '\n')
    print('============================================')


def get_information_error_code():

    print('============================================')
    err_code = input('\nInput the error code you want more information about: ')
    print('Error code {} is: {}\n'.format(err_code, os.strerror(int(err_code))))
    print('============================================')
