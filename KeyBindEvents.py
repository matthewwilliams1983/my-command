class KeyBinds:

    # Bind return and the up and down arrow keys
    def __init__(self):

        self.key_bind_list = []

    def bind_key(self, widget=None, key_code="", binding_function=None, arg_to_pass_to_function=""):

        widget.bind(
            key_code, lambda event:
            binding_function(event, arg_to_pass_to_function)
        )

        self.key_bind_list.append(key_code + ' ' + binding_function.__name__)