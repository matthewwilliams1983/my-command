import re


class InputStringOps:

    def __init__(self, where_to_split=None):

        self.__where_to_split = where_to_split
        self.__split_string = None

    def split_string_in_two(self, string_input):

        self.__split_string = re.split(self.__where_to_split, string_input)

        self.__split_string = list(map(lambda x: x.strip(' '), self.__split_string))
        self.__split_string = list(filter(lambda x: x != '', self.__split_string))

        if len(self.__split_string) < 2:
            self.__split_string.append('')

        return self.__split_string
